var mongojs = require('mongojs');

var databaseUrl = 'mongodb://localhost:27017/APIDB';
var collections = ['users', 'work'];

var connect = mongojs(databaseUrl, collections);

module.exports = {
    connect: connect
};