var app = require('express')();

var mongojs = require('./route/db');

var user = require('./users');


var bodyParser = require('body-parser');

var port = process.env.PORT || 1515;
var db = mongojs.connect;


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.get('/', function(req,res){
    res.send('<h1> Hello Test NodeJS express</h1>');
});
// mongodb db.js//
app.get('/userdb', function (req, res) {
    db.users.find(function(err, docs) {
        res.json(docs);
    });
});

app.get('/userdb/:id', function (req, res) {
    var id = parseInt(req.params.id);

    db.users.findOne({id: id}, function(err, docs) {
        res.json(docs);
    });
});

app.post('/insertuserdb', function (req, res) {
    var newuserdb = req.body;
    db.users.insert(newuserdb, function(err, docs) {
        res.json(newuserdb);
    });

});

app.post('/modifyuserdb', function (req, res) {
db.users.findAndModify({
    query: { username: 'Zoro' },
    update: { $set: req.body },
    new: true
}, function(err, doc, lastErrorObject) {
    res.json(err);
});

});


app.post('/removeuserdb', function (req, res) {
db.users.remove({id : 3}, function(err, docs) {
    res.send("Romove Complete!");
});
});


///local DB (users.js)//
app.get('/index', function(req,res){
    res.send('<h1> Hello Page1</h1>');
});

app.get('/user', function(req,res){
    res.json(user.findAll());
});

app.get('/user/:id', function(req,res){
    var id = req.params.id;
    res.json(user.findById(id));
});

app.post('/newuser',function(req,res){
    var json = req.body;
    res.send(user.addNewUser(json));
});

app.put('/updateuser/:id', function(req,res){
    var id = req.params.id;
    var json = req.body;
    res.send(user.UpdateUser(id, json));
});

app.listen(port,function(){
    console.log('Starting index.js on port ' + port);
});

